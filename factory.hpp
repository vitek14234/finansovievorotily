#ifndef FACTORY_HPP
#define FACTORY_HPP
enum TypesOfFactory { Normal, Automate, Construction };
class Factory{
    TypesOfFactory type;
    int daysBeforeEndofConstruction;
    Factory(TypesOfFactory type, int days=0) : type(type), daysBeforeEndofConstruction(days)
    {};
};

#endif // FACTORY_HPP

