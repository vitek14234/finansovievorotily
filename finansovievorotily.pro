#-------------------------------------------------
#
# Project created by QtCreator 2015-01-29T12:08:35
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = finansovievorotily
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    company.cpp

HEADERS += \
    factory.hpp \
    company.hpp \
    credit.hpp
